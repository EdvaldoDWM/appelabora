define( function ( require ) {

	"use strict";

	return {
		app_slug : 'x-congresso-mossoro',
		wp_ws_url : 'http://xcongressomossoro.hospedagemdesites.ws/wp-appkit-api/x-congresso-mossoro',
		wp_url : 'http://xcongressomossoro.hospedagemdesites.ws',
		theme : 'q-android',
		version : '0.0.0',
		app_title : 'X Congresso Mossoró',
		app_platform : 'android',
		gmt_offset : 0,
		debug_mode : 'off',
		auth_key : 'fzsxya25qddpkakucu03uxjoavpr4c8jdjapmifgi28np6piqm8ibtmg6nf7ou9d',
		options : {"refresh_interval":0},
		theme_settings : [],
		addons : []
	};

});
